from flask import Flask
from os import getenv


app = Flask(__name__)

@app.route("/")
def index():
    return "hello world"

@app.route("/new")
def new_endpoint():
    return "new endpoint"

@app.route("/finish")
def finish():
    return "Chegamos ao final do curso, Parabens!!!"

if __name__ == "__main__":
    app.run(host="0.0.0.0", port=getenv('PORT'))