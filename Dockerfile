# imagem base
FROM python:3.10.4-slim as base

# Cria pasta
RUN mkdir /packages

# Copia os arquivos de config do poetry
COPY pyproject.toml poetry.lock /packages/

# move para o diretorio
WORKDIR /packages


# atualiza pip instala poetry
RUN pip install --upgrade pip && \
    pip install poetry


# gera arquivo requerimentos e desistala poetry e instala dependencias
RUN poetry config virtualenvs.create false && poetry install

# Cria um novo estagio
FROM base

# Copia os binarios do pacote python para dentro da nova imagem
COPY --from=base /packages /usr/local/

# move para diretorio app
WORKDIR /app

# copia arquivos para dentro do container
COPY . /app

# expoe porta 5000
EXPOSE 5000

# executa aplicação
CMD ["python3", "app.py"]
